const hre = require("hardhat");
const { URLSearchParams } = require('url');

const NAME = "Box Hosh";
const SYMBOL = "BX";
const URI = "https://cloudflare-ipfs.com/ipfs/QmXert5ThmdsZuYA8TfScxHZBCZh2brBCRxmZTa7tZYF47";
const DEPLOYER = "0x9be29dD2bcA0a42F738D9478295bd0e2dAff433F";
async function main() {

  console.log('deployment start.');
  
  global.URLSearchParams = URLSearchParams;
  // Init Contract Token
  const Box = await hre.ethers.getContractFactory("Box");

  // Deploy Contract 
  console.log("------------- Start Deployed  ---------------");
  const box = await Box.deploy(NAME, SYMBOL, URI);  
  console.log("box: ", box.address);
  console.log("------------- Deployed  ---------------");
  await box.deployed();

  console.log(`deployer address: ${box.deployTransaction.from}`);
  console.log(`gas price: ${box.deployTransaction.gasPrice}`);
  console.log(`gas used: ${box.deployTransaction.gasLimit}`);

  console.log('getting contract');
  const B = await ethers.getContractFactory("Box");
  const b = await B.attach(box.address);
  console.log('setting value');
  await b.mint(DEPLOYER, URI);
  console.log('setup done');
  

  await hre.run(
    "verify:verify", {
      address: box.address,
      constructorArguments: [
        NAME, SYMBOL, URI
      ]
  }
  );

}

main()
    .then(() => process.exit(0))
    .catch(error => {
      console.error(error);
      process.exit(1);
    });

