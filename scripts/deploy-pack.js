const hre = require("hardhat");
const { URLSearchParams } = require('url');

const TEMPLATE = "0x22E77e09B495D4CDd0616Bb1b728D862840548A2";
const BOX = "0x1313e5e9c43B5c5Afeb80dc8d369C9670D254CC2";

async function main() {

  console.log('deployment start.');
  
  global.URLSearchParams = URLSearchParams;
  // Init Contract Token
  const Pack = await hre.ethers.getContractFactory("Pack");

  // Deploy Contract 
  console.log("------------- Start Deployed  ---------------");
  const pack = await Pack.deploy(TEMPLATE, BOX);  
  console.log("pack: ", pack.address);
  console.log("------------- Deployed  ---------------");
  await pack.deployed();

  console.log(`deployer address: ${pack.deployTransaction.from}`);
  console.log(`gas price: ${pack.deployTransaction.gasPrice}`);
  console.log(`gas used: ${pack.deployTransaction.gasLimit}`);

  await hre.run(
    "verify:verify", {
      address: pack.address,
      constructorArguments: [
        TEMPLATE, BOX
      ]
  }
  );

}

main()
    .then(() => process.exit(0))
    .catch(error => {
      console.error(error);
      process.exit(1);
    });

