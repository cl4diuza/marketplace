const hre = require("hardhat");
const { URLSearchParams } = require('url');

const URI = "ipfs://https://cloudflare-ipfs-1.com/ipfs/QmXert5ThmdsZuYA8TfScxHZBCZh2brBCRxmZTa7tZYF47"

async function main() {

  console.log('deployment start.');
  
  global.URLSearchParams = URLSearchParams;
  // Init Contract Token
  const Template = await hre.ethers.getContractFactory("Template");

  // Deploy Contract 
  console.log("------------- Start Deployed  ---------------");
  const template = await Template.deploy();  
  console.log("box: ", template.address);
  console.log("------------- Deployed  ---------------");
  await template.deployed();
  
  await hre.run(
    "verify:verify", {
      address: template.address
  }
  );

}

main()
    .then(() => process.exit(0))
    .catch(error => {
      console.error(error);
      process.exit(1);
    });

