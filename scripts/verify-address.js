const { ethers, upgrades } = require('hardhat');
const { URLSearchParams } = require('url');
async function main() {
  
  global.URLSearchParams = URLSearchParams;
  
  await hre.run(
    "verify:verify", {
      address: "0x021d0dB0Be5d5b20167B32CdEAb1CC35E6ed8457"
  }
  );
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
