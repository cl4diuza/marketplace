const hre = require("hardhat");
const { URLSearchParams } = require('url');

const ROUTER = "0x9Ac64Cc6e4415144C455BD8E4837Fea55603e5c3";

const PEACH = "0x5081F7d88Cba44e88225Cb177c41e16c1635e22A";
const BUSD = "0x78867bbeef44f2326bf8ddd1941a4439382ef2a7";
const WBNB = "0xae13d989dac2f0debff460ac112a837c89baa7cd";

const pathPEACHToWBNB = [PEACH, WBNB];
const pathPEACHToBUSD = [PEACH, WBNB, BUSD];

const pathWBNBToPEACH = [WBNB, PEACH];
const pathWBNBToBUSD = [WBNB, BUSD];

const pathBUSDToWBNB = [BUSD, WBNB];
const pathBUSDToPEACH = [BUSD, PEACH];


async function main() {

  console.log('deployment start.');
  
  global.URLSearchParams = URLSearchParams;
  // Init Contract Token
  const Zap = await hre.ethers.getContractFactory("Zap");

  // Deploy Contract 
  console.log("------------- Start Deployed  ---------------");
  const zap = await Zap.deploy(ROUTER, WBNB);  
  console.log("zap: ", zap.address);
  console.log("------------- Deployed  ---------------");
  await zap.deployed();

  console.log(`deployer address: ${zap.deployTransaction.from}`);
  console.log(`gas price: ${zap.deployTransaction.gasPrice}`);
  console.log(`gas used: ${zap.deployTransaction.gasLimit}`);

  console.log('getting contract');
  const Z = await ethers.getContractFactory("Zap");
  const z = await Z.attach(zap.address);
  console.log('setting value');
  await z.addRoute(0, pathPEACHToWBNB);
  await z.addRoute(1, pathPEACHToBUSD);
  await z.addRoute(2, pathWBNBToPEACH);
  await z.addRoute(3, pathWBNBToBUSD);
  await z.addRoute(4, pathBUSDToWBNB);
  await z.addRoute(5, pathBUSDToPEACH);
  console.log('setup done');
  

  await hre.run(
    "verify:verify", {
      address: zap.address,
      constructorArguments: [
        ROUTER, WBNB
      ]
  }
  );

}

main()
    .then(() => process.exit(0))
    .catch(error => {
      console.error(error);
      process.exit(1);
    });

