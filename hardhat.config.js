require("@nomiclabs/hardhat-waffle");
require('@nomiclabs/hardhat-ethers');
require("@nomiclabs/hardhat-etherscan");
require('@openzeppelin/hardhat-upgrades');

task("accounts", "Prints the list of accounts", async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});
module.exports = {
  solidity: {
    compilers: [
      {
        version: "0.8.13",
        settings: {
          optimizer: {
            enabled: true
          }
        }
      },
      {
        version: "0.8.10",
        settings: {
          optimizer: {
            enabled: true
          }
        }
      }
    ]
  },
  networks: {
    bsc_mainnet: {
      url: "https://bsc-dataseed1.binance.org",
      allowUnlimitedContractSize: true
    },
    bsc_testnet: {
      url: "https://data-seed-prebsc-1-s1.binance.org:8545/",
      allowUnlimitedContractSize: true,
      gasLimit: 20000,
      accounts: ["08284fdf120936db9c2077b22c2ea7462a9738f491ca7044303aca8c7920006c"]
    }
  },
  etherscan: {
    apiKey: "PPSEVHETR6WQR6Y6M7J1718S9XSX27MBHC"
  }
};
