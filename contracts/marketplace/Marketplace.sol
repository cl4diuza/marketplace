// SPDX-License-Identifier: MIT
pragma solidity 0.8.13;

import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721Receiver.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";

//TODO 
//Future ขายเป็น pack
contract Marketplace is IERC721Receiver, AccessControl {
  using Counters for Counters.Counter;
  using SafeERC20 for IERC20;
  bytes32 public constant ADMIN_ROLE = keccak256("ADMIN_ROLE");
    
  Counters.Counter public _itemId;
  Counters.Counter public _itemsSold;
  Counters.Counter public _itemOrder;
  Counters.Counter public _auctionItemId;
  Counters.Counter public _whitelistNftCount;
  Counters.Counter public _whitelistTokenCount;
  Counters.Counter public _blacklistSeller;
  address payable owner;

  mapping(uint256 => MarketItem) public idToMarketItem;
  mapping(uint256 => MarketItem) public orderHistories;
  mapping(uint256 => AuctionItem) public idToAuctionItem;
  mapping(uint256 => BidderItem) public idToBitterItem;
  mapping(address => bool) public whitelistNftAddress;
  mapping(address => bool) public whitelistTokenAddress;
  mapping(address => bool) public blacklistSeller;

  struct MarketItem {
    uint256 tokenId;
    uint256 price;
    address payable seller;
    address payable owner;
    address tokenAddress;
    address nftAddress;
    address nftOwnerAddress;
    bool sold;
    bool cancel;
  }

  struct AuctionItem {
    uint256 tokenId;
    uint256 minPrice;
    uint256 bidPrice;
    uint256 date;
    address payable auctioneer;
    address payable owner;
    address tokenAddress;
    address nftAddress;
    address nftOwnerAddress;
    bool success;
    bool cancel;
  }

  struct BidderItem {
    uint256 itemId;
    uint256 highPrice;
    uint256 date;
    address bidder;
  }

  event MarketItemCreated (
    uint256 indexed tokenId,
    uint256 price,
    address seller,
    address owner,
    address tokenAddress,
    address nftAddress,
    bool sold,
    bool cancel
  );

  event SoldItem (
    uint256 indexed tokenId,
    uint256 price,
    address seller,
    address owner,
    address tokenAddress,
    address nftAddress
  );

  event CancelSellItem (
    uint256 indexed tokenId,
    uint256 price,
    address seller,
    address tokenAddress,
    address nftAddress,
    bool cancel
  );

  event Auction_Item (
    uint256 tokenId,
    uint256 minPrice,
    uint256 bidPrice,
    uint256 date,
    address payable auctioneer,
    address payable owner,
    address tokenAddress,
    address nftAddress,
    address nftOwnerAddress,
    bool success,
    bool cancel
  );

  event Bidder_Item (
    uint256 itemId,
    uint256 highPrice,
    uint256 date,
    address bidder
  );

  event AuctionSuccess (
    uint256 itemId,
    uint256 minPrice,
    address payable auctioneer,
    address payable owner
  );

  event CancelAuction (
    uint256 itemId,
    uint256 tokenId,
    bool cancel
  );

  event SetWhitelist (
    address contactAddress,
    bool status
  );

  event SetBlacklist (
    address contactAddress,
    bool status
  );

  constructor(){
    owner = payable(msg.sender);
  }

  function setAdmin(address admin) public {
    require(msg.sender == owner);
    _setupRole(ADMIN_ROLE, admin);
  }

  function setBlacklishSeller(address sellerAddress, bool status) public {
    require(hasRole(ADMIN_ROLE, msg.sender), "Caller is not a admin");
    blacklistSeller[sellerAddress] = status;
      
    if(status) {
      _blacklistSeller.increment();
    } else {
      _blacklistSeller.decrement();
    }

    emit SetBlacklist (
      sellerAddress,
      status
    );
  }

  function setWhitelishNftAddress(address nftAddress) public {
    require(hasRole(ADMIN_ROLE, msg.sender), "Caller is not a admin");
    whitelistNftAddress[nftAddress] = true;
    _whitelistNftCount.increment();

    emit SetWhitelist(
      nftAddress,
      true
    );
  }

  function setWhitelistTokenAddress(address tokenAddress) public {
    require(hasRole(ADMIN_ROLE, msg.sender), "Caller is not a admin");
    whitelistTokenAddress[tokenAddress] = true;
    _whitelistTokenCount.increment();

    emit SetWhitelist(
      tokenAddress,
      true
    );
  }

  function onERC721Received(address, address, uint256, bytes memory) public pure returns (bytes4) {
    return this.onERC721Received.selector;
  }

  function sellItem(
    uint256 tokenId,
    uint256 price,
    address nftAddress,
    address nftOwnerAddress,
    address tokenAddress
  ) public {
    require(price > 0, "Price must be at least 1 wei");
    require(whitelistNftAddress[nftAddress] == true, "This nft is not authorized.");
    require(blacklistSeller[msg.sender] == false, "Blacklist seller");

    if(tokenAddress != address(0)){
      require(whitelistTokenAddress[tokenAddress] == true, "This token is not authorized.");
    }

    uint256 id =_itemId.current();
    idToMarketItem[id] =  MarketItem(
      tokenId,
      price,
      payable(msg.sender),
      payable(address(this)),
      tokenAddress,
      nftAddress,
      nftOwnerAddress,
      false,
      false
    );
    _itemId.increment();
    IERC721(nftAddress).safeTransferFrom(msg.sender,address(this),tokenId);

    emit MarketItemCreated(
      tokenId,
      price,
      msg.sender,
      address(this),
      nftAddress,
      tokenAddress,
      false,
      false
    );
  }
    /* Creates the sale of a marketplace item */
    /* Transfers ownership of the item, as well as funds between parties */
    //FIX change owner when buy item
  function buyItem(
    uint256 _itemID
  ) public payable returns(uint256){
    MarketItem memory marketItem = idToMarketItem[_itemID];
    IERC20 tokenAddress = IERC20(marketItem.tokenAddress);
    //หักตัง
    require(marketItem.price != 0,"Item not exist.");
    require(marketItem.cancel == false,"this item cancel the sale.");
    require(marketItem.sold == false,"This item has been sold.");

    bool nativeToken = false;
    if (marketItem.tokenAddress == address(0)){
      require(msg.value >= marketItem.price, "not enough money(native)");
      nativeToken = true;
    } else {
      require(tokenAddress.balanceOf(msg.sender) >= marketItem.price, "not enough money");
    }

    uint256 toSellerMoney = marketItem.price * 900000 / 1000000;
    uint256 toMarketMoney = marketItem.price * 50000 / 1000000;
    uint256 toOwnerNftMoney = marketItem.price * 50000 / 1000000;
  
    if (nativeToken){
      payable(marketItem.seller).transfer(toSellerMoney);
      payable(owner).transfer(toMarketMoney);
      payable(marketItem.nftOwnerAddress).transfer(toOwnerNftMoney);
    } else {
      tokenAddress.safeTransferFrom(msg.sender,marketItem.seller,toSellerMoney);
      tokenAddress.safeTransferFrom(msg.sender,owner,toMarketMoney);
      tokenAddress.safeTransferFrom(msg.sender,marketItem.nftAddress,toOwnerNftMoney);
    }

    //โอน nft
    IERC721(marketItem.nftAddress).safeTransferFrom(address(this),msg.sender,marketItem.tokenId);
    //get history id
    uint256 orderId = _itemOrder.current();
    _itemsSold.increment();
    //save
    idToMarketItem[_itemID] =  MarketItem(
      marketItem.tokenId,
      marketItem.price,
      marketItem.seller,
      payable(msg.sender),
      marketItem.tokenAddress,
      marketItem.nftAddress,
      marketItem.nftOwnerAddress,
      true,
      false
    );

    orderHistories[orderId] = MarketItem(
      marketItem.tokenId,
      marketItem.price,
      marketItem.seller,
      payable(msg.sender),
      marketItem.tokenAddress,
      marketItem.nftAddress,
      marketItem.nftOwnerAddress,
      true,
      false
    );
    //log
    emit SoldItem (
      marketItem.tokenId,
      marketItem.price,
      marketItem.seller,
      payable(msg.sender),
      marketItem.tokenAddress,
      marketItem.nftAddress
    );
    _itemOrder.increment();
    return orderId;
  }

  function cancelSell(uint256 _itemID) public returns(bool){
    MarketItem memory marketItem = idToMarketItem[_itemID];
    require(msg.sender == marketItem.seller, "Your not seller of this item.");
    require(marketItem.sold != true, "This item has been sold.");
    require(marketItem.cancel != true, "This item has been cancel.");

    //return item to seller
    IERC721(marketItem.nftAddress).safeTransferFrom(address(this),msg.sender,marketItem.tokenId);

    //change cancel status
    idToMarketItem[_itemID] =  MarketItem(
      marketItem.tokenId,
      marketItem.price,
      marketItem.seller,
      payable(msg.sender),
      marketItem.tokenAddress,
      marketItem.nftAddress,
      marketItem.nftOwnerAddress,
      false,
      true
    );

    //log
    emit CancelSellItem (
      marketItem.tokenId,
      marketItem.price,
      marketItem.seller,
      marketItem.tokenAddress,
      marketItem.nftAddress,
      marketItem.cancel
    );

    return true;
  }

  function auctionItem(
    uint256 tokenId,
    uint256 minPrice,
    uint256 bidPrice,
    uint256 date,
    address nftAddress,
    address nftOwnerAddress,
    address tokenAddress
  )public {
    require(minPrice > 0, "Price must be at least 1 wei");
    require(whitelistNftAddress[nftAddress] == true, "This nft is not authorized.");
    require(blacklistSeller[msg.sender] == false, "Blacklist seller");

    if(tokenAddress != address(0)){
      require(whitelistTokenAddress[tokenAddress] == true, "This token is not authorized.");
    }

    uint256 id = _auctionItemId.current();

    idToAuctionItem[id] = AuctionItem(
      tokenId,
      minPrice,
      bidPrice,
      date,
      payable(msg.sender),
      payable(address(this)),
      tokenAddress,
      nftAddress,
      nftOwnerAddress,
      false,
      false
    );

    _auctionItemId.increment();
    IERC721(nftAddress).safeTransferFrom(msg.sender,address(this),tokenId);

    emit Auction_Item (
      tokenId,
      minPrice,
      bidPrice,
      date,
      payable(msg.sender),
      payable(address(this)),
      tokenAddress,
      nftAddress,
      nftOwnerAddress,
      false,
      false
    );
  }

  //TODO collect money from bidder when have new bidder refund money to old bidder
  function bidderItem(uint256 _itemID, uint256 money) public payable returns(bool){
    AuctionItem memory auctionItem = idToAuctionItem[_itemID];
    BidderItem memory oldBidder = idToBitterItem[_itemID];
    IERC20 tokenAddress = IERC20(auctionItem.tokenAddress);
    uint256 timeNow = block.timestamp;

    require(money > auctionItem.minPrice + auctionItem.bidPrice, "Please bitter than min price + bid price.");
    require(msg.sender != auctionItem.auctioneer,"You are auctioneer");
    require(timeNow <= auctionItem.date, "End auction time.");
    require(auctionItem.success != true, "This auction is success.");
    require(auctionItem.cancel != true, "This auction is cancel.");

    bool nativeToken = false;
    if (auctionItem.tokenAddress == address(0)){
      require(msg.value == money, "not enough money(native)");
      nativeToken = true;
    } else {
      require(tokenAddress.balanceOf(msg.sender) > money, "not enough money");
    }

    if (nativeToken){
      if (oldBidder.bidder != address(0)) {
        payable(oldBidder.bidder).transfer(oldBidder.highPrice);
      }
    } else {
      tokenAddress.safeTransferFrom(msg.sender,address(this),money);
      if (oldBidder.bidder != address(0)) {
        tokenAddress.safeTransferFrom(address(this),oldBidder.bidder,oldBidder.highPrice);
      }
    }

    idToBitterItem[_itemID] = BidderItem(
      _itemID,
      money,
      timeNow,
      payable(msg.sender)
    );

    idToAuctionItem[_itemID] = AuctionItem(
      auctionItem.tokenId,
      money,
      auctionItem.bidPrice,
      auctionItem.date,
      auctionItem.auctioneer,
      auctionItem.owner,
      auctionItem.tokenAddress,
      auctionItem.nftAddress,
      auctionItem.nftOwnerAddress,
      auctionItem.success,
      auctionItem.cancel
    );

    emit Bidder_Item(
      _itemID,
      money,
      timeNow,
      payable(msg.sender)
    );

    return true;
  }

  function endAuctionItem(uint256 _itemID) public {
    AuctionItem memory auction_Item = idToAuctionItem[_itemID];
    BidderItem memory bidder = idToBitterItem[_itemID];
    IERC20 tokenAddress = IERC20(auction_Item.tokenAddress);
    uint256 timeNow = block.timestamp;
      
    require(msg.sender == auction_Item.auctioneer || msg.sender == bidder.bidder, "You are't owner of auction");
    require(timeNow > auction_Item.date, "It's not yet time to end the auction.");
    require(auction_Item.cancel == false,"this item cancel the auction.");
    require(auction_Item.success == false,"This item has been success.");

    bool nativeToken = false;

    if (auction_Item.tokenAddress == address(0)){
      nativeToken = true;
    }

    uint256 toSellerMoney = bidder.highPrice * 900000 / 1000000;
    uint256 toMarketMoney = bidder.highPrice * 50000 / 1000000;
    uint256 toOwnerNftMoney = bidder.highPrice * 50000 / 1000000;

    if (nativeToken){
      payable(auction_Item.auctioneer).transfer(toSellerMoney);
      payable(auction_Item.nftOwnerAddress).transfer(toOwnerNftMoney);
      payable(owner).transfer(toMarketMoney);
    } else {
      tokenAddress.transfer(auction_Item.auctioneer, toSellerMoney);
      tokenAddress.transfer(auction_Item.nftOwnerAddress, toOwnerNftMoney);
      tokenAddress.transfer(owner, toMarketMoney);
    }

    IERC721(auction_Item.nftAddress).safeTransferFrom(address(this),bidder.bidder,auction_Item.tokenId);

    idToAuctionItem[_itemID] = AuctionItem(
      auction_Item.tokenId,
      auction_Item.minPrice,
      auction_Item.bidPrice,
      auction_Item.date,
      auction_Item.auctioneer,
      auction_Item.owner,
      auction_Item.tokenAddress,
      auction_Item.nftAddress,
      auction_Item.nftOwnerAddress,
      true,
      auction_Item.cancel
    );

    emit AuctionSuccess(
      auction_Item.tokenId,
      auction_Item.minPrice,
      auction_Item.auctioneer,
      payable(bidder.bidder)
    );
  }

  function cancelAuctionItem(uint256 _itemID) public {
    AuctionItem memory auction_Item = idToAuctionItem[_itemID];
    BidderItem memory bidder = idToBitterItem[_itemID];
    IERC20 tokenAddress = IERC20(auction_Item.tokenAddress);

    require(msg.sender == auction_Item.auctioneer, "Your not auctioneer of this item.");
    require(auction_Item.success != true, "This item has been success.");
    require(auction_Item.cancel != true, "This item has been cancel.");

    bool nativeToken = false;
    
    if (auction_Item.tokenAddress == address(0)){
      nativeToken = true;
    }
    if (nativeToken){
      payable(bidder.bidder).transfer(bidder.highPrice);
    } else {
      tokenAddress.safeTransferFrom(address(this),bidder.bidder,bidder.highPrice);
    }

    //return item to auctioneer
    IERC721(auction_Item.nftAddress).safeTransferFrom(address(this),msg.sender,auction_Item.tokenId);

    //change cancel status
    idToAuctionItem[_itemID] = AuctionItem(
      auction_Item.tokenId,
      auction_Item.minPrice,
      auction_Item.bidPrice,
      auction_Item.date,
      auction_Item.auctioneer,
      auction_Item.owner,
      auction_Item.tokenAddress,
      auction_Item.nftAddress,
      auction_Item.nftOwnerAddress,
      auction_Item.success,
      true
    );

    //log
    emit CancelAuction (
      auction_Item.tokenId,
      _itemID,
      true
    );
  }

  function tranferItem(
    uint256 _itemID
  )public {
    MarketItem memory a = idToMarketItem[_itemID];
    IERC721(a.nftAddress).safeTransferFrom(address(this),msg.sender,a.tokenId);
  }

  function withdrawToken(address _token,uint256 amount) public{
    require(msg.sender == owner);
    IERC20 token = IERC20(_token);
    
    if (_token == address(0)){
      payable(msg.sender).transfer(amount);
    } else {
      token.transfer(msg.sender, amount);
    }

  }

  function getAllMarketItem(uint id) external view returns (
    uint[] memory tokenId,
    uint[] memory price,
    address[] memory seller,
    address[] memory holder,
    address[] memory tokenAddress,
    address[] memory nftAddress,
    bool[] memory sold
  ){
    uint count = _itemId.current();
    uint[] memory tokenIds = new uint[](count);
    uint[] memory prices = new uint[](count);
    address[] memory sellers = new address[](count);
    address[] memory owners = new address[](count);
    address[] memory tokenAddresses = new address[](count);
    address[] memory nftAddresses = new address[](count);
    bool[] memory solds = new bool[](count);
    for (uint i = 0; i < count; i++) {
      MarketItem memory market = idToMarketItem[i];
        tokenIds[i] = market.tokenId;
        prices[i] = market.price;
        sellers[i] = market.seller;
        owners[i] = market.owner;
        tokenAddresses[i] = market.tokenAddress;
        nftAddresses[i] = market.nftAddress;
        solds[i] = market.sold;
    }
    return (tokenIds,prices,sellers,owners,tokenAddresses,nftAddresses,solds);
  }
}