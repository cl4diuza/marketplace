// SPDX-License-Identifier: MIT
pragma solidity 0.8.13;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract Template is Ownable {
    using SafeMath for uint256;
    using SafeERC20 for IERC20;

    uint256 templateIdCounter;

    mapping(uint256 => Model) public templates;

    mapping(address => Model[]) public userTemplates;

    struct Model {
        uint256 id;
        string ipfsPath;
        address nftAddress;
    }

    event TemplateCreated(address indexed user, uint256 indexed templateId, uint256 indexed userTemplateIndex);

    function createTemplate(string calldata _ipfsPath, address _nftAddress) external returns (uint256) {
        require (keccak256(abi.encodePacked(_ipfsPath)) != keccak256(abi.encodePacked("")), "ERROR_EMPTY_IPFS_PATH");

        uint256 templateId = templateIdCounter;
        uint256 userTemplateId = userTemplates[_msgSender()].length;

        templates[templateId] = Model(templateId, _ipfsPath, _nftAddress);
        userTemplates[_msgSender()].push(Model(templateId, _ipfsPath, _nftAddress));

        templateIdCounter ++;
        emit TemplateCreated(_msgSender(), templateId, userTemplateId);
        return templateId;
    }

    function getTemplatesByUser(address user) external view returns (uint256[] memory ids, string[] memory templatePaths, address[] memory templateAddresses) {
        Model[] memory models = userTemplates[user];
        uint256[] memory templateIds = new uint256[](models.length);
        string[] memory paths = new string[](models.length);
        address[] memory addresses = new address[](models.length);
        for (uint256 i = 0; i < models.length; i ++) {
            templateIds[i] = models[i].id;
            paths[i] = models[i].ipfsPath;
            addresses[i] = models[i].nftAddress;
        }

        return (templateIds, paths, addresses);
    }

    function getAddressesByTemplates(uint256[] calldata _templateIds) external view returns (address[] memory templateAddresses) {
        address[] memory addresses = new address[](_templateIds.length);
        for (uint256 i = 0; i < _templateIds.length; i ++) {
            addresses[i] = templates[_templateIds[i]].nftAddress;
        }
        return addresses;
    }

    function getTotalTemplates() external view returns (uint256) {
        return templateIdCounter;
    }

    // get uri
    function getTemplateURIs(uint256[] calldata _templateIds) external view returns (string[] memory uris) {
        string[] memory URIs = new string[](_templateIds.length);
        for (uint256 i = 0; i < _templateIds.length; i ++) {
            uint256 _templateId = _templateIds[i];
            require(isExist(_templateId), "ERROR_INVALID_BLUEPRINT_ID");
            uint256 index;
            bool isTemplateIdExist = false;
            for (uint256 j = 0; j < templateIdCounter; j ++) {
                if (templates[j].id == _templateId) {
                    index = j;
                    isTemplateIdExist = true;
                    break;
                }
            }
            require(isTemplateIdExist, "TEMPLATE NOT EXIST");
            URIs[i] = string(abi.encodePacked("ipfs://", templates[index].ipfsPath));
        }
        return URIs;
    }

    function isExist(uint256 _templateId) public view returns (bool) {
        return templates[_templateId].nftAddress != address(0);
    }
}