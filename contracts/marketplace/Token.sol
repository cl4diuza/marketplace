// // SPDX-License-Identifier: MIT
// pragma solidity 0.8.13;

// import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
// import "@openzeppelin/contracts/access/AccessControlEnumerable.sol";

// import "./Template.sol";
// import "./Pack.sol";

// /**
//  * @title Token
//  * @notice The ERC721 that is hold by packs. Tokens are created from author's blueprints
//  */
// contract Token is ERC721Enumerable, AccessControlEnumerable {
//     /**
//      * @notice Struct holding relevant information from the minting process
//      */
//     struct MintData {
//         bytes32 purchaseOrderId;
//         uint256 index; // index in the pack
//     }

//     bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");

//     Template private templateContract;
//     Pack private packContract;

//     /**
//      * @dev Map of tokens to their mint data
//      */
//     mapping(uint256 => MintData) private _mintData;

//     /**
//      * @notice Emitted when the 'tokenId' is minted and transfered to the 'owner' in accordance to purchase order 'purchaseOrderId'
//      */
//     event Minted(uint256 indexed tokenId, address owner, bytes32 indexed purchaseOrderId);

//     constructor (
//         string memory name__,
//         string memory symbol__,
//         address _packAddress
//     ) ERC721(name__, symbol__) { 
//         _setupRole(MINTER_ROLE, _packAddress);
//         templateContract = new Template();
//         packContract = Pack(_packAddress);
//     }

//     function templateContractAddress() external view returns(address) {
//         return address(templateContract);
//     }

//     function mintFromPack(
//             bytes32 purchaseOrderId, 
//             uint256 index
//     ) public virtual returns (uint256) {

//         uint256 tokenId = totalSupply();
//         _safeMint(to, tokenId);
//         _mintData[tokenId].purchaseOrderId = purchaseOrderId;
//         _mintData[tokenId].index = index;

//         emit Minted(tokenId, to, purchaseOrderId);
//         return tokenId;
//     }

//     /**
//      * @dev See {IERC721Metadata-tokenURI}.
//      */
//     function tokenURI(uint256 tokenId) public view virtual override returns (string memory) {
//         require(_exists(tokenId), "ERROR_INVALID_TOKEN_ID");

//         uint256 blueprintId = packContract.mintedBlueprint(
//             _mintData[tokenId].purchaseOrderId, 
//             _mintData[tokenId].index
//         );

//         return templateContract.blueprintURI(blueprintId);
//     }

//     /**
//      * @dev See {IERC165-supportsInterface}.
//      */
//     function supportsInterface(bytes4 interfaceId) public view virtual override(AccessControlEnumerable, ERC721Enumerable) returns (bool) {
//         return super.supportsInterface(interfaceId);
//     }

// }