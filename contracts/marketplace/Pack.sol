// SPDX-License-Identifier: MIT
pragma solidity 0.8.13;

import "@openzeppelin/contracts/utils/Context.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721Receiver.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

interface ITemplate {
    function getTemplatesByUser(address user) external view returns (uint256[] memory ids, string[] memory templatePaths, address[] memory templateAddresses);

    function getAddressesByTemplates(uint256[] calldata _templateIds) external view returns (address[] memory templateAddresses);

    function getTemplateURIs(uint256[] calldata _templateIds) external view returns (string[] memory uris);
}

interface IBox {
    function mint(address to, string memory uri) external returns (uint256);

    function burns(uint256 tokenId) external;

    function safeTransferFrom(
        address from,
        address to,
        uint256 tokenId
    ) external;
}

interface IAsset {
    function mint(address to, string memory uri) external;

    function totalSupply() external view returns (uint256);
}

contract Pack is Context, IERC721Receiver {
    using SafeERC20 for IERC20;
    

    ITemplate template;
    IBox box;
    
    uint256 private _packCounter;
    uint256 private _slotCounter;
    uint256 private _requestId;

    mapping(address => uint256[]) public userCollections;

    mapping(uint256 => PackCollection) public packCollection;

    mapping(uint256 => uint256) public tokenPacks;

    mapping(uint256 => Asset[]) public openPackHistories;

    mapping(uint256 => uint256) public requestIdPack;

    mapping(uint256 => SlotCollection) public slotCollection;

    mapping(uint256 => uint256[]) public packSlots;
    // mapping(address => Order) private orders;

    struct Asset {
        address nftAddress;
        uint256 tokenId; 
    }

    struct PackCollection {
        string packIpfsPath;
        uint8 maxSupply;
        uint8 slot;
        uint256 price;
        address owner;
        address tokenAddress;
    }

    struct Order {
        address buyer;
        uint256 packId;
        uint256 tokenId;
        uint256 price;
    }

    struct SlotCollection {
        uint256 packId;
        uint8 times;
        uint256[] templates;
        uint256[] chances;
    }

    event PackCreated(address indexed owner, uint256 indexed packId, uint256 userCollectionIndex);

    event OrderCreated(address indexed buyer, uint256 indexed packId, uint256 indexed orderId);

    event OpenPack(address addressSender, uint256 _requestId ,uint256 tokenId, uint256 randomTimes);

    event CreateSlot(uint256 packId,uint256 slotId, uint8 times, uint256[] templates, uint256[] chances);


    constructor (address _templateAddress, address _boxAddress)
    { 
        template = ITemplate(_templateAddress);
        box = IBox(_boxAddress);
    }

    function onERC721Received(address, address, uint256, bytes memory) public pure returns (bytes4) {
        return this.onERC721Received.selector;
    }

    function createPack(
        string calldata packIpfsPath,
        address tokenAddress,
        uint8 maxSupply,    
        uint8 slot,
        uint256 price
    ) external returns (uint256) {        
        // check empty
        require (keccak256(abi.encodePacked(packIpfsPath)) != keccak256(abi.encodePacked("")), "ERROR_EMPTY_IPFS_PATH");

        // keeping index
        uint256 packId = _packCounter;
        _packCounter += 1;
        uint256 userCollectionsIndex = userCollections[msg.sender].length;
        
        // put all these in global pack collection
        PackCollection storage pack = packCollection[packId];
        pack.packIpfsPath = packIpfsPath;
        pack.price = price;
        pack.maxSupply = maxSupply;
        pack.owner = msg.sender;
        pack.tokenAddress = tokenAddress;
        pack.slot = slot;

        // put these in user collection
        userCollections[msg.sender].push(packId);

        emit PackCreated(msg.sender, packId, userCollectionsIndex);
        return packId;
    }

    function createSlot(uint256 packId,uint8 times,uint256[] memory templates,uint256[] memory chances) external {
        require(templates.length == chances.length, "ERROR_INVALID_TEMPLATE_LENGTH_CHANCES_LENGHT");
        uint256 sumChance = 0;
        for(uint256 i = 0; i < chances.length; i++){
            sumChance = sumChance + chances[i];
        }
        require(sumChance == 1e4, "CHANCE SUM ERROR");

        uint256 slotId = _slotCounter;
        
        
        SlotCollection storage slotCollections = slotCollection[slotId];
        slotCollections.packId = packId;
        slotCollections.times = times;
        slotCollections.templates = templates;
        slotCollections.chances = chances;

        packSlots[packId].push(slotId);
        
        emit CreateSlot(
            packId,
            slotId,
            times,
            templates,
            chances
        );
        
        _slotCounter += 1;
    }

    function buyPack(uint256 packId) payable public returns (uint256) {
        PackCollection storage pack = packCollection[packId];
        require(pack.price != 0, "COLLECTION NOT EXIST");
        bool nativeToken = false;
        if (pack.tokenAddress == address(0)) {
            require(msg.value >= pack.price, "ERROR_INVALID_AMOUNT");
            nativeToken = true;
        }

        require(pack.maxSupply > 0, "PACK IS ALREADY SOLD OUT");

        uint256 mintedTokenId = 0;

        // mint nft box , this a MINTER ROLE
        uint256 tokenId = box.mint(msg.sender, pack.packIpfsPath);
        
        // assign to keep track tokenId
        tokenPacks[tokenId] = packId;

        if (nativeToken) {
            payable(pack.owner).transfer(pack.price);
        } else  {
            // pay with other token
            IERC20(pack.tokenAddress).safeTransferFrom(msg.sender, pack.owner, pack.price);
        }
        
        emit OrderCreated(msg.sender, packId, mintedTokenId);
        pack.maxSupply --;
        return mintedTokenId;
    }

    function openPack(uint256 tokenId) public {
        // get nft from caller
        box.safeTransferFrom(msg.sender, address(this), tokenId);
        // burn it
        box.burns(tokenId);
        // assign to keep track tokenId
        requestIdPack[_requestId] = tokenId;
        
        uint256 packId = tokenPacks[tokenId];
        uint256[] memory slotInPack = packSlots[packId];
        uint256 randomTimes = 0;

        for (uint256 i = 0; i < slotInPack.length; i++){
            SlotCollection memory slot = slotCollection[slotInPack[i]];
            randomTimes += slot.times;
        }
        // get co-relate template, address and chances
        emit OpenPack(
            msg.sender,
            _requestId,
            tokenId,
            randomTimes
        );

        _requestId++;

        // save the result
        // ---- this contract must have MINTER ROLE for each nft address
        // tranfer the nft to caller
    }

    function fullFillRandomness(uint256 requestId,uint256[] memory randoms) public{
        uint256 tokenId = requestIdPack[requestId]; //1
        uint256 packId = tokenPacks[tokenId];
        uint256[] memory slotInPack = packSlots[packId]; // [0,1,2,3]
        PackCollection storage pack = packCollection[packId];

        uint256 rewardIndex;
        uint8 thisRandom = 0;
        //each slot
        for (uint256 i = 0; i < pack.slot; i++){
                
            SlotCollection memory slot = slotCollection[slotInPack[i]];

            uint256[] memory chances = slot.chances;
            //uint256[] memory templates = slot.templates; //1 -> [0,1,2,3]
            address[] memory nftsAddress = template.getAddressesByTemplates(slot.templates);
            string[] memory URIs = template.getTemplateURIs(slot.templates);
                
            //each time
            for (uint256 j = 0; j < slot.times; j++){
                //item result
                for (uint256 k = 0; k < slot.chances.length; k ++) { 
                    uint256 chance = chances[k]; // [5,10,30,55]

                    if (randoms[thisRandom] > chance) {
                        continue;
                    } else {
                        rewardIndex = k;
                    }

                }

                IAsset(nftsAddress[rewardIndex]).mint(msg.sender, URIs[rewardIndex]);
                thisRandom++;
            }
        }
    }

}